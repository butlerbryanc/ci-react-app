'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _reactRouterDom = require('react-router-dom');

var _favicon512x = require('./favicon-512x512.png');

var _favicon512x2 = _interopRequireDefault(_favicon512x);

var _thing = require('./thing.png');

var _thing2 = _interopRequireDefault(_thing);

var _api = require('../api');

var _user = require('../actions/user');

var userActions = _interopRequireWildcard(_user);

require('./FirstPage.css');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FirstPageParent = function (_Component) {
  _inherits(FirstPageParent, _Component);

  function FirstPageParent() {
    _classCallCheck(this, FirstPageParent);

    return _possibleConstructorReturn(this, (FirstPageParent.__proto__ || Object.getPrototypeOf(FirstPageParent)).apply(this, arguments));
  }

  _createClass(FirstPageParent, [{
    key: 'componentWillMount',
    value: function () {
      var _ref = _asyncToGenerator(_regenerator2.default.mark(function _callee() {
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.props.userActions.set({ text: 'loading' });

                this._handleData('firstPage');

              case 2:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: '_handleData',
    value: function () {
      var _ref2 = _asyncToGenerator(_regenerator2.default.mark(function _callee2(key) {
        var staticContext, text, _text, _ref3, _text2;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                staticContext = this.props.staticContext;

                if (!(staticContext && staticContext.data[key])) {
                  _context2.next = 7;
                  break;
                }

                text = staticContext.data[key].text;

                this.props.userActions.set({ text: text });
                staticContext.head.push(_react2.default.createElement('meta', { name: 'description', content: "Some description: " + text }));
                _context2.next = 23;
                break;

              case 7:
                if (!staticContext) {
                  _context2.next = 11;
                  break;
                }

                staticContext.data[key] = this._getData();
                _context2.next = 23;
                break;

              case 11:
                if (!(!staticContext && window.DATA[key])) {
                  _context2.next = 17;
                  break;
                }

                _text = window.DATA[key].text;

                this.props.userActions.set({ text: _text });
                window.DATA[key] = null;
                _context2.next = 23;
                break;

              case 17:
                if (staticContext) {
                  _context2.next = 23;
                  break;
                }

                _context2.next = 20;
                return this._getData();

              case 20:
                _ref3 = _context2.sent;
                _text2 = _ref3.text;

                this.props.userActions.set({ text: _text2 });

              case 23:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function _handleData(_x) {
        return _ref2.apply(this, arguments);
      }

      return _handleData;
    }()
  }, {
    key: '_getData',
    value: function () {
      var _ref4 = _asyncToGenerator(_regenerator2.default.mark(function _callee3() {
        var staticContext, Api, myApi, _ref5, text;

        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                staticContext = this.props.staticContext;
                Api = staticContext ? staticContext.api.MainApi : _api.MainApi;
                myApi = new Api();
                _context3.next = 5;
                return myApi.getMain();

              case 5:
                _ref5 = _context3.sent;
                text = _ref5.text;
                return _context3.abrupt('return', { text: text });

              case 8:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function _getData() {
        return _ref4.apply(this, arguments);
      }

      return _getData;
    }()
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(FirstPage, this.props);
    }
  }]);

  return FirstPageParent;
}(_react.Component);

var FirstPage = function (_Component2) {
  _inherits(FirstPage, _Component2);

  function FirstPage() {
    _classCallCheck(this, FirstPage);

    return _possibleConstructorReturn(this, (FirstPage.__proto__ || Object.getPrototypeOf(FirstPage)).apply(this, arguments));
  }

  _createClass(FirstPage, [{
    key: 'render',
    value: function render() {
      var b64 = this.props.staticContext ? 'wait for it' : window.btoa('wait for it');
      var _props$user = this.props.user,
          text = _props$user.text,
          email = _props$user.email;

      return _react2.default.createElement(
        'div',
        { className: 'bold FirstPage' },
        _react2.default.createElement(
          'h2',
          null,
          'First Page'
        ),
        _react2.default.createElement('img', { src: _favicon512x2.default, alt: 'favicon-big' }),
        _react2.default.createElement('img', { src: _thing2.default, alt: 'thing-inline' }),
        _react2.default.createElement(
          'p',
          null,
          'Email: ' + email
        ),
        _react2.default.createElement(
          'p',
          null,
          'Database / delayed Text: ' + text
        ),
        _react2.default.createElement(
          'p',
          null,
          'b64: ' + b64
        ),
        _react2.default.createElement(
          _reactRouterDom.Link,
          { to: '/second' },
          'Second'
        )
      );
    }
  }]);

  return FirstPage;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    user: state.user
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    userActions: (0, _redux.bindActionCreators)(userActions, dispatch)
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FirstPageParent);