'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var prefix = 'USER/';

var SET = exports.SET = prefix + 'SET';
var RESET = exports.RESET = prefix + 'RESET';