'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.render = render;
exports.renderHead = renderHead;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _server = require('react-dom/server');

var _reactRedux = require('react-redux');

var _reactRouterDom = require('react-router-dom');

var _App = require('./containers/App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function render(req, store, context) {
  return (0, _server.renderToString)(_react2.default.createElement(
    _reactRedux.Provider,
    { store: store },
    _react2.default.createElement(
      _reactRouterDom.StaticRouter,
      {
        location: req.url,
        context: context
      },
      _react2.default.createElement(_App2.default, null)
    )
  ));
}

function renderHead(context) {
  return context.head.map(function (h) {
    return (0, _server.renderToStaticMarkup)(h);
  }).join('');
}